enum ORIENTATIONS {
    North = "North",
    West = "West",
    South = "South",
    East = "East"
}

export class MarsRover {
    orientation = ORIENTATIONS.North;
    coordinate = [0,0];

    move(command: string) {

        let commands = command.split("");

        commands.forEach( singleCommand => {
            if (singleCommand === "l") {
                this.turnleft();
            } else if (singleCommand === "r") {
                this.turnRight();
            } else if (singleCommand === "f") {
                if (this.orientation === ORIENTATIONS.North) {
                    this.coordinate[1]++;
                } else if (this.orientation === ORIENTATIONS.East) {
                    this.coordinate[0]++;
                } else if (this.orientation === ORIENTATIONS.West) {
                    this.coordinate[0]--;
                } else if (this.orientation === ORIENTATIONS.South) {
                    this.coordinate[1]--;
                }
            }
        });
    }

    private turnRight() {
        switch (this.orientation) {
            case ORIENTATIONS.North :
                this.orientation = ORIENTATIONS.East;
                break;
            case ORIENTATIONS.East:
                this.orientation = ORIENTATIONS.South;
                break;
            case ORIENTATIONS.South:
                this.orientation = ORIENTATIONS.West;
                break;
            case ORIENTATIONS.West:
                this.orientation = ORIENTATIONS.North;
                break;
            default:
                break;
        }
    }

    turnleft(): void {
        switch (this.orientation) {
            case ORIENTATIONS.North:
                this.orientation = ORIENTATIONS.West;
                break;

            case ORIENTATIONS.West:
                this.orientation = ORIENTATIONS.South;
                break;

            case ORIENTATIONS.South:
                this.orientation = ORIENTATIONS.East;
                break

            case ORIENTATIONS.East:
                this.orientation = ORIENTATIONS.North;
                break;
        }
    }
}
