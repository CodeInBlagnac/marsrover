import "mocha";
import * as assert from "assert";
import {MarsRover} from "../src/MarsRover";

describe("MarsRover should ", () => {

    let marsRover;

    beforeEach(() => {
        marsRover = new MarsRover();
    });

    it("start oriented North with position 0,0", () =>{
        assert.strictEqual(marsRover.orientation, "North");
        assert.deepStrictEqual(marsRover.coordinate, [0,0]);
    });

    describe("Tests only for left", () => {
        it("be oriented West when rotate left from North", () => {
            marsRover.move("l");
            assert.strictEqual(marsRover.orientation, "West");
        });

        it("be oriented South when rotate left twice from North", () => {
            marsRover.move("ll");
            assert.strictEqual(marsRover.orientation, "South")
        });

        it("be oriented East when rotate left thrice from North", () => {
            marsRover.move("lll");
            assert.strictEqual(marsRover.orientation, "East");
        });

        it("be oriented North when rotate left four times from North", () => {
            marsRover.move("llll");
            assert.strictEqual(marsRover.orientation, "North");
        });
    });

    describe("Tests only right", () => {

        it( "should make a full turn right", () => {
            marsRover.move("r");
            assert.strictEqual(marsRover.orientation, "East")
            marsRover.move("r");
            assert.strictEqual(marsRover.orientation, "South")
            marsRover.move("r");
            assert.strictEqual(marsRover.orientation, "West")
            marsRover.move("r");
            assert.strictEqual(marsRover.orientation, "North")
        });

        it("be oriented South when rotate right twice from North", () => {
            marsRover.move("rr");
            assert.strictEqual(marsRover.orientation, "South")
        });
    });


    describe("Tests left and right", () => {

        it("be oriented North after several rotations", () => {
            marsRover.move("rrllrlrrlrll");
            assert.strictEqual(marsRover.orientation, "North")
        });
    });

    describe("Test move forward", () => {

        it("should move 1 forward", () => {
            marsRover.move("f");
            assert.strictEqual(marsRover.orientation, "North");
            assert.deepStrictEqual(marsRover.coordinate, [0,1]);
        });
        it("should move 2 forward", () => {
            marsRover.move("ff");
            assert.strictEqual(marsRover.orientation, "North");
            assert.deepStrictEqual(marsRover.coordinate, [0,2]);
        });
        it("should move 3 forward", () => {
            marsRover.move("fff");
            assert.strictEqual(marsRover.orientation, "North");
            assert.deepStrictEqual(marsRover.coordinate, [0,3]);
        });
        it("should turn right then move 1 forward", () => {
            marsRover.move("rf");
            assert.strictEqual(marsRover.orientation, "East");
            assert.deepStrictEqual(marsRover.coordinate, [1,0]);
        });
        it("should turn twice right then move 1 forward", () => {
            marsRover.move("rrf");
            assert.strictEqual(marsRover.orientation, "South");
            assert.deepStrictEqual(marsRover.coordinate, [0,-1]);
        });
        it("should move 1 forward then turn right", () => {
            marsRover.move("fr");
            assert.strictEqual(marsRover.orientation, "East");
            assert.deepStrictEqual(marsRover.coordinate, [0,1]);
        });
        it("should turn left then move 1 forward", () => {
            marsRover.move("lf");
            assert.strictEqual(marsRover.orientation, "West");
            assert.deepStrictEqual(marsRover.coordinate, [-1,0]);
        });

    });



});


